include <gears/gears.scad> 

$fn = 360;

difference() {
    union() {
        translate([0,0,7]) {
            translate([0,0,3.5]) {
                intersection() {
                    bevel_gear(modul=2, tooth_number=40,  partial_cone_angle=45,tooth_width=10, bore=30, pressure_angle=20, helix_angle=0);
                    cylinder(h=9.5,d=80);
                }
            };
            cylinder(h=3.5,d1=70,d2=77);
        }
        cylinder(h=16.5,d=39.8);
    };
    union() {
        translate([0,0,-1]) {
            cylinder(h=20,d=20);
            translate([-3,0,0]) {
                cube([6,13,20]);
            }
        }
    };
}

