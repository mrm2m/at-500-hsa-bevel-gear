OpenSCAD bevel gear model for AT 500 HSA
========================================

The AT 500 HSA is a multifunctional device for metal milling, turning and
drilling. To switch between the different tasks there's a clutch in the
transmission to either couple the engine directpy on the lathe or change
direction to the vertical mill. This change of direction is done via two
bevel gears. One of these gears is made of steal, the other one which is
easy to access and therefor easy to replace is made of plastic. This makes
absolutely sense: The plastic gear will wear off while the hard to replace
metal gear will last forever.

The AT 500 HSA is about 40 years old by now and spare parts are hard to find.
The company Wemas which sold it back than changed their field of business and
even for the clarke CL500M which is probably identical in construction and
still advertized there is no such spare part available (part number HT3000103).

There are several ideas of how to replace this gear with parts from other
machines or by generic bevel gears, but all these solutions require the
replacement of both gears.

I still had my original part just missing some teeth and designed a replacement
for 3d printing with help of the excelent gear generator library for OpenSCAD.
I gave it a first test as inexpansive PLA print with a fill of only 20%. I
planned to replace it by a more solid Nylon print if it fits. Actually it
fitted very well and I did not have to replace it yet as it turned out to be
much more durable than I thought in the first place. I used it to mill
stainless steel parts and so far it does not show any physical damage.

Clone Repository
----------------

The model requires the gears repository to build the 3d model. To receive all
required sources clone repository and submodule:

```
git clone https://chaos.expert/mrm2m/at-500-hsa-bevel-gear.git
cd at-500-hsa-bevel-gear
gir submodule update --init
```

Print 3d model
--------------

If you want to print the model without changes you can use the precompiled STL
model at https://chaos.expert/mrm2m/at-500-hsa-bevel-gear/-/raw/master/bevel-gear.stl?inline=false.

